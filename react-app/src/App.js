import './App.css';
import { useEffect, useState } from 'react';
import NewCustomerForm from './components/NewCustomerForm';
import ListCustomerForm from './components/ListCustomerForm';

function App() {
  
  const [didListUpdate, setDidUpdate] = useState({ didListUpdate: false });
 
  const setStateForUpdate = (name) => {
    setDidUpdate(name);
  }

  return (
    <div className="App">
      <table>
        <tr>
          <td width="30%">
            <h2>Add new Customer</h2>
            <NewCustomerForm setStateForUpdate={setStateForUpdate} />
          </td>
          <td>
            <h2>List of Customers</h2>
            <ListCustomerForm didListUpdate={didListUpdate} />
          </td>
        </tr>
      </table>
    </div>
  );
}

export default App;
