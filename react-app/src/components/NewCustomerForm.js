import React, { useEffect, useState } from "react";
import axios from "axios";
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import * as Constants from '../constants';


function NewCustomerForm(props) {
    const [hubConnection, setHubConnection] = useState(null);
    
    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: ""
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        axios.post(Constants.REACT_APP_WEB_API_CUSTOMER_CREATE_URL, {
            firstName: formData.firstName,
            lastName: formData.lastName,
            email: formData.email,
            preferenceIds: [
                "76324c47-68d2-472d-abb8-33cfa8cc0c84"
            ]
        })
            .then(res => {
                if (hubConnection) {
                    hubConnection.invoke("Send", "The form is updated");
                }
            });
    }

    const handleChange = (e) => {
        const newdata = { ...formData }
        newdata[e.target.id] = e.target.value
        setFormData(newdata);
    }

    useEffect(() => {
        createHubConnection();
    }, []);

    const createHubConnection = async () => {
        const hubConnection = new HubConnectionBuilder().withUrl(Constants.REACT_APP_WEB_API_CUSTOMER_HUB).build();
        try {
            await hubConnection.start();
        } catch (e) {
            console.log("data", e);
        }
        setHubConnection(hubConnection);
    }

    useEffect(() => {
        if (hubConnection) {
            hubConnection.on("ReceiveMessage", (message) => {
                console.log(message);
                props.setStateForUpdate({ didUpdate: true });
            });
        }
    }, [hubConnection]);

    return (
        <div>
            <form onSubmit={(e) => handleSubmit(e)}>
                <br/><input onChange={(e) => handleChange(e)} id="firstName" value={formData.firstName} placeholder="First name" type="text" />
                <br/><input onChange={(e) => handleChange(e)} id="lastName" value={formData.lastName} placeholder="Last name" type="text" />
                <br/><input onChange={(e) => handleChange(e)} id="email" value={formData.email} placeholder="Email" type="email" />
                <br/><button>Submit</button>
            </form>
        </div>
    )
}

export default NewCustomerForm;
