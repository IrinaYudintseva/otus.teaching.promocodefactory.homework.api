import React, { useEffect, useState } from "react";
import axios from "axios";
import * as Constants from '../constants';
import 'devextreme/dist/css/dx.light.css';
import { DataGrid } from 'devextreme-react';

function ListCustomerForm(props) {

    const [data, setData] = useState({ customers: [] });
  
    useEffect(() => {
      async function fetchData() {
        const cust_query = await axios.post(
          Constants.REACT_APP_GRAPHQL_API, {
          query: Constants.GET_CUSTOMERS_QUERY
        }
        );
        const result = cust_query.data.data;
        setData({ customers: result.customers })
        return result.customers;
      };
  
      fetchData();
    }, [props.didListUpdate]);
  
    return (
        <div>
          <DataGrid
            dataSource={data.customers}
            keyExpr={data.customers.id}>
          </DataGrid>
        </div>
    )
}

export default ListCustomerForm;
