﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class GraphQLQuery
    {

            private readonly DataContext _dataContext;

            public GraphQLQuery(DataContext dataContext)
            {
                _dataContext = dataContext;
            } 

            public IQueryable<Customer> Customers
            {
                get { return _dataContext.Customers; }
            }

            public IQueryable<Preference> Preferences
            {
                get { return _dataContext.Preferences; }
            }
    }

}
